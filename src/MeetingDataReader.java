import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MeetingDataReader {
	public Map<Pair<String, String>, Integer> readRawData(String rawDataFilePath) {
		Map<Pair<String, String>, Integer> meetingCountMap = new HashMap<>();

		BufferedReader rawDataReader = null;
		try {
			rawDataReader = new BufferedReader(new FileReader(rawDataFilePath));
			String row;
			while ((row = rawDataReader.readLine()) != null) {
				if (row.trim().equals("")) {
					System.out.println("[WARNING] Raw data has empty line.");
					continue;
				}
				Pair<String, String> meeting = getMeetingInfo(row);
				addCount(meetingCountMap, meeting);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rawDataReader != null) {
					rawDataReader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return meetingCountMap;
	}

	private Pair<String, String> getMeetingInfo(String row) {
		String[] persons = row.split("\t");
		if (persons.length != 2) {
			throw new IllegalDataException("[ERROR] Raw data has unavailable data. row : \n\t" + row);
		}

		String personA = persons[0];
		String personB = persons[1];

		if (personA.equals(personB)) {
			throw new IllegalDataException("[ERROR] Raw data has unavailable data. person " + personA + " meets person " + personB + ".");
		}

		Pair<String, String> meetingInfo = personA.compareTo(personB) < 1 ? new Pair(personA, personB) : new Pair<>(personB, personA);
		System.out.println("[Meeting Info] " + meetingInfo);

		return meetingInfo;
	}

	private void addCount(Map<Pair<String, String>, Integer> meetingCountMap, Pair<String, String> meeting) {
		if (meetingCountMap.containsKey(meeting)) {
			Integer meetingCount = meetingCountMap.get(meeting);
			meetingCountMap.put(meeting, ++meetingCount);
		} else {
			meetingCountMap.put(meeting, 1);
		}
	}

	public static class IllegalDataException extends RuntimeException {
		public IllegalDataException(String errorMessage) {
			super(errorMessage);
		}
	}

}
