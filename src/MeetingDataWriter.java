import au.com.bytecode.opencsv.CSVWriter;
import javafx.util.Pair;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.*;

public class MeetingDataWriter {
	private static final int BOM_FIRST_BYTE = 0xEF;
	private static final int BOM_SECOND_BYTE = 0xBB;
	private static final int BOM_THIRD_BYTE = 0xBF;

	public void writeCsv(Map<Pair<String, String>, Integer> meetingCountMap, String outputFilePath) {
		List<String> personList = getSortedPersonList(meetingCountMap);

		CSVWriter csvWriter = null;
		try {
			csvWriter = getCsvWriter(outputFilePath);
			writeTableHead(csvWriter, personList);
			writeTableBody(csvWriter, personList, meetingCountMap);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (csvWriter != null) {
					csvWriter.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private List<String> getSortedPersonList(Map<Pair<String, String>, Integer> meetingCountMap) {
		Set<String> personSet = new HashSet<>();
		Set<Pair<String, String>> pairs = meetingCountMap.keySet();
		for (Pair<String, String> pair : pairs) {
			personSet.add(pair.getKey());
			personSet.add(pair.getValue());
		}

		List<String> persons = new ArrayList<>(personSet);
		Collections.sort(persons);
		return persons;
	}

	private CSVWriter getCsvWriter(String filePath) throws IOException {
		FileOutputStream fileOutputStream = new FileOutputStream(filePath);
		OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, Charset.forName("UTF-8"));
		CSVWriter csvWriter = new CSVWriter(outputStreamWriter);

		// UTF-8 CSV 를 잘 읽기 위해 UTF-8 BOM 을 붙임
		fileOutputStream.write(BOM_FIRST_BYTE);
		fileOutputStream.write(BOM_SECOND_BYTE);
		fileOutputStream.write(BOM_THIRD_BYTE);

		return csvWriter;
	}

	private void writeTableHead(CSVWriter csvWriter, List<String> persons) {
		String[] tableHead = getTableHead(persons);
		csvWriter.writeNext(tableHead);
	}

	private String[] getTableHead(List<String> persons) {
		List<String> columnNames = new ArrayList<>();
		columnNames.add("");
		columnNames.addAll(persons);

		String[] columnStringArray = new String[columnNames.size() + 1];
		return columnNames.toArray(columnStringArray);
	}

	private void writeTableBody(CSVWriter csvWriter, List<String> persons, Map<Pair<String, String>, Integer> meetingCountMap) {
		for (int row = 2; row < persons.size() + 2; row++) {
			List<String> rowData = new ArrayList<>();
			for (int column = 1; column < persons.size() + 2; column++) {
				if (column == 1) {
					rowData.add(persons.get(row - 2));
				} else if (row >= column) {
					rowData.add("-");
				} else {
					String personA = persons.get(row - 2);
					String personB = persons.get(column - 2);
					Pair<String, String> meetInfo = new Pair<>(personA, personB);
					Integer count = meetingCountMap.get(meetInfo);
					if (count == null) {
						count = 0;
					}
					rowData.add(String.valueOf(count));
				}
			}
			csvWriter.writeNext(rowData.toArray(new String[rowData.size()]));
		}
	}
}
