import javafx.util.Pair;

import java.util.Map;

public class Main {
	private static final String RAW_DATA_FILE_PATH = "input/meeting_data.txt";
	private static final String OUTPUT_CSV_FILE_PATH = "output/meeting_data.csv";

	public static void main(String[] args) {
		MeetingDataReader meetingDataReader = new MeetingDataReader();
		Map<Pair<String, String>, Integer> meetingCountMap = meetingDataReader.readRawData(RAW_DATA_FILE_PATH);

		MeetingDataWriter meetingDataWriter = new MeetingDataWriter();
		meetingDataWriter.writeCsv(meetingCountMap, OUTPUT_CSV_FILE_PATH);
	}

}
